import datetime
import json
import matplotlib.pyplot as plt

def storerun(title, variables=None, dic_globals=None, plot = True):
    """Store the plot and the dico of variables.
    Param variables of the shape: "var1, var2, var3", will be splitted
    """
    now = datetime.datetime.now().strftime("%d:%m:%Y:_:%H:%M:%S")
    path = 'runs/' + title +  now
    if plot:
        plt.savefig(path + '.png')
    if variables is not None and dic_globals is not None:
        dic_var = {i:str(dic_globals[i]) for i in variables.split(', ')}
        with open(path+'json', 'w+') as f:
            json.dump(dic_var, f)