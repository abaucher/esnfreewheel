# File utils.py, define some useful functions

# RANDOM
import numpy as np

def unif(low=-1, high=1, size=1):
    """Returns random numbers between low and high"""
    return np.random.random(size=size)*(high - low) + low

# LATEX
import tikzplotlib
from os import system

def save_tex(name, compiler=False):
    """Save a figure as latex tikz, then compile it if asked"""
    tikzplotlib.clean_figure()
    with open(f'latex/{name}.tex', "w+") as f:
        if compiler:
            f.write(tikzplotlib.Flavors.latex.preamble())
            f.write('\\begin{document}')
        f.write(tikzplotlib.get_tikz_code())
        if compiler:
            f.write('\\end{document}')    
    system(f'pdflatex --output-directory latex/  {name}.tex')
    system(f'cp latex/{name}.pdf img/')
    