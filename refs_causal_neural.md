Références pour causalité, et apprentissage de dynamiques réseaux neuronaux, trouvées sur google
* https://aip.scitation.org/doi/abs/10.1063/1.5134845
* https://www.pik-potsdam.de/members/kurths/publikationen/2020/tang-2020-5-0016505.pdf
* https://www.nature.com/articles/s41598-021-87411-8
